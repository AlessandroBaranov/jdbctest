package net.proselyte.jdbctest.model;

public interface Model {
    public String toString();
}
